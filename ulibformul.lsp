(defun horizontal_WGroove (code grooveParams note / grooveWid grooveDept grooveFrontDist cutTwoRailGroove_params altkanal1pt ustkanal1pt ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody)
	(mapcar 'set '(grooveWid grooveDept grooveFrontDist) grooveParams)
	(setq cutTwoRailGroove_params (getnth 0 (getRailParams)) )		
	(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cutTwoRailGroove_params) 

	(setq mozel_mUnit T 
		altkanal1pt  (replace (- ad_et grooveDept) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) ) 
		ustkanal1pt	 (replace (- dHeight ad_et) 2 (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid) ) )
	)
	
	(mapcar '(lambda(x) (createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ grooveDept ad_et) 0.0) (list 0.0 (+ grooveDept ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle x "railGroove" "" nil "PANELPCONN")) (list altkanal1pt ustkanal1pt))
	(princ)
)

(defun vertical_WGroove (code grooveParams note / grooveWid grooveDept grooveFrontDist makeSidesRayGroove_params  solkanal1pt sagkanal1pt heightGap ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody)
	(mapcar 'set '(grooveWid grooveDept grooveFrontDist) grooveParams)
	
	(setq makeSidesRayGroove_params (getnth 0 (getRailParams)) )		
	(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) makeSidesRayGroove_params) 
	
	(setq heightGap (abs (- (getnth 2 ptMinBody) (getnth 2 ptMaxBody))))
	
	(setq mozel_mUnit T 
		solkanal1pt  (polar (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid)) (- DEG_180 dAngle) grooveDept)
		sagkanal1pt  (polar (polar ptOffSet (+ DEG_90 dAngle) (+ grooveFrontDist grooveWid)) dAngle (- dist ad_et))
	)
	
	(mapcar '(lambda(x) (createGPanel (list (list 0.0 0.0 0.0) (list (+ grooveDept ad_et) 0.0 0.0) (list (+ grooveDept ad_et) heightGap 0.0) (list 0.0 heightGap 0.0)) (list 0.0 0.0 0.0 0.0) grooveWid 0.0 dAngle x "railGroove" "" nil "PANELPCONN")) (list solkanal1pt sagkanal1pt))
	(princ)

)

(defun cagberk_skm40 (code note  /  cagberk_skm40_params cagberk_skm40_params newPanelData tempZ)
		(setq cagberk_skm40_params (getnth 0 (getRailParams)) )		
		(mapcar 'set '(ptMinA  ptMaxA  ptOffSet dist dDepth dAngle dHeight ptTmpMin ptTmpMax ptMinBody ptMaxBody) cagberk_skm40_params) 
		
		(setq mozel_mUnit T 
			altkanal1pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.9 ) ) 
			altkanal2pt  (replace (- ad_et 1.2) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
			ustkanal1pt	 (replace (- dHeight ad_et (- ad_et 1.2) ) 2 (polar ptOffSet (+ DEG_90 dAngle) 1.25 ) )
			ustkanal2pt  (replace (- dHeight ad_et (- ad_et 1.2)) 2 (polar ptOffSet (+ DEG_90 dAngle) 3.0 ) )
		)
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    0.8 0.0 dAngle altkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0) 0.8 0.0 dAngle altkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0(+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal1pt "railGroove" "" nil "PANELPCONN")
		
		(createGPanel (list (list 0.0 0.0 0.0) (list dist 0.0 0.0) (list dist (+ ad_et ad_et) 0.0) (list 0.0 (+ ad_et ad_et) 0.0)) (list 0.0 0.0 0.0 0.0)    (- ad_et 1.2) 0.0 dAngle ustkanal2pt "railGroove" "" nil "PANELPCONN")
		
		(princ)
)

;standart raf
(defun xmf_raf_offset (yuk y inputet) 
	(+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet)
)

(defun xmf_raf_offset_list (yuk y inputet / xmf_raf_offset_result) 
	(repeat y
		(setq xmf_raf_offset_result (cons (+ (* (- yuk (* inputet y)) (/ 1.0 (+ y 1.0))) inputet) xmf_raf_offset_result))
	)
	xmf_raf_offset_result
)

;Biti� mod�l� raf aralar� e�itlemek i�in xmf_raf kullan
;x:Bu raf dolap i�inde ka��nc� 
;y:Bu dolap i�inde ka� raf var
;x ve y reelsay� olmal� 1.0 gibi 
(defun GM1_Raf (x y ) 
	(+ (* (- curDivH (* ad_et y)) (/ x (+ y 1.0)))  (* (- x 1.0) ad_et))
)

;Biti� mod�l� dikme aralar� e�itlemek i�in kullan
;x:Bu dikme dolap i�inde ka��nc� 
;y:Bu dolap i�inde ka� dikme var
;x ve y reelsay� olmal� 1.0 gibi 
(defun GM1_Dikme (x y ) 
	(+ (* (- unitW (* ad_Et 2) (* ad_et y)) (/ x (+ y 1.0)))  (* (- x 1.0) ad_et))
)

;Standar Cam raf
(defun GM1_CRaf (x y ) 
	(+ (*  curDivH (/ x (+ y 1.0)))  (* (- x 1.0) 0.8))
)

;Aventos i�i iki raf olursa �steki raf�n dolab�n �st i�inden mesafesi. �rnek HF 040 {H2} 
(defun avn_ust_raf ()  
	(list (* curDivH 0.31))
)

;Aventos i�i iki raf olursa alttaki raf�n dolab�n �st i�inden mesafesi 
(defun avn_alt_raf ()
	(list (* curDivH 0.6431))
)

;_______
;Tek b�l�ml� dolab�n b�l�m y�ksekli�i
(defun bh_1bd ()
	 remCTH
)

;Tek b�l�ml� dolab�n kapak y�ksekli�i
(defun kh_1bd ()
	 (- unitH ZTop ZBot)
)

;________
;4 e�it �ekmeceli dolab�n 1. b�l�m y�ksekli�i
(defun bh_4s_#1c ()
	(+ (- (kh_4s_#1c) ad_et) ZTop (* ZBtwn 0.5))
)

;4 e�it �ekmeceli dolab�n 1. kapak y�ksekli�i
(defun kh_4s_#1c ()
	(* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.25)
)

;D�rde b�l�ml� sistemin 1 �ekmeceli 1 kapakl� dolab�n 3de 4e tekab�l eden alt b�l�m y�ksekli�i
(defun bh_4s_2+3+4#2b ()
	(- (kh_4s_2+3+4#2b) (- ad_et ZBot (* ZBtwn 0.5)))
)

;D�rde b�l�ml� sistemin 1 �ekmeceli 1 kapakl� dolab�n 3de 4e tekab�l eden alt b�l�m kapak y�ksekli�i
(defun kh_4s_2+3+4#2b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.75)  (* 2.0 ZBtwn))
)

;_________
;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m y�ksekli�i
(defun bh_4s_1+2_#1b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZTop (* ZBtwn 0.5))
)

;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m�n kapak y�ksekli�i
(defun kh_4s_1+2_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;D�rte b�l�ml� sistemin 2de 4e tekab�l eden b�l�m�n alt b�l�m y�ksekli�i
(defun bh_4s_3+4_#2b ()
	(+ (- (kh_4s_1+2_#1b) ad_et) ZBot (* ZBtwn 0.5))
)

;D�rte b�l�ml� 3 �ekmeceli dolap orta b�lme 

(defun bh_4s_#2b ()
	(+ (kh_4s_#1c) ZBtwn)
)
;_____

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap orta b�lme y�ksekli�i
(defun bh_4s_3:8b_#1b ()
	(+ (kh_4s_3:8b_#1b) ZBtwn)
)

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap orta b�lme kapak y�ksekli�i
(defun kh_4s_3:8b_#1b ()
	(+ (* (- unitH ZTop ZBtwn ZBtwn ZBtwn ZBot) 0.375) (* ZBtwn 0.5))
)

;3 �ekmeceli 1 dar iki e�it �ekmeceli dolap alt b�lme y�ksekli�i 

(defun bh_4s_3:8b_#2b ()
	(+ (- (kh_4s_3:8b_#1b) ad_et) ZBot (* ZBtwn 0.50))
)

;_______
;D�rde b�l�ml� sistemin 4 e�it �ekmeceli ���nc� b�l�me tekab�l eden b�l�m y�ksekli�i, kapak birinci b�l�mdeki gibidir.
(defun bh_4s_#3c ()
	(+ (kh_4s_#1c) ZBtwn)
)

;D�rde b�l�ml� sistemin 4 e�it �ekmeceli d�rd�nc� b�l�me tekab�l eden b�l�m y�ksekli�i, kapak birinci b�l�mdeki gibidir.
(defun bh_4s_#4c ()
	(- (kh_4s_#1c) (- ad_et ZBot (* ZBtwn 0.5)))
)

;Ankastre F�r�n alt kapak
(defun ANK_K ()
	(+ remCTH (- (* 1.5 ad_et) ZBot (* ZBtwn 0.5) ) )
)

;____
;�st kalkar kapakl� dolap �st b�l�m 
(defun bh_2b_ust_#1b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZTop (* ZBtwn 0.5)) (* ad_et 0.5))
)

;�st kalkar kapakl� dolap kapa�� 
(defun kh_2b_ust_#1b ()
	(* (- unitH ZTop ZBtwn ZBot) 0.50)
)

;�st kalkar kapakl� dolap alt b�l�m 
(defun bh_2b_ust_#2b ()
	(- (+ (- (kh_2b_ust_#1b) ad_et) ZBot (* ZBtwn 0.5)) (* ad_et 0.5))
)

;____--______
;Asp. dolab� kapa��
(defun kh_1bd_asp ()
	(+ (- unitH ZTop ZBot) (cm2cu 13.9))
)

;_________
;Alt dolap sabit mod�ller i�in raf ayar�
(defun xmf_SabitAlt_Raf_Kural (inputet / temph)
	(setq temph (- ad_adh ad_et ad_et))
	(cond 
		((> ad_ADH 74.0) (list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
		((> ad_ADH 70.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil)
	)
)

;�st dolap sabit mod�ller i�in raf ayar�
(defun xmf_SabitUst_Raf_Kural (inputUDH inputet / temph)
	(setq temph (- inputUDH ad_et ad_et))
	(cond
		((> inputUDH 95.0) (list (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet) (xmf_raf_offset temph 3.0 inputet)))
		((> inputUDH 71.0) (list (xmf_raf_offset temph 2.0 inputet) (xmf_raf_offset temph 2.0 inputet)))
		((> inputUDH 39.0) (list (xmf_raf_offset temph 1.0 inputet)))
		(T nil)
	)
)

;GM1 mod�l y�ksekli�ine g�re rad adedi ayar�
(defun gm1_Ust_Raf_Kural (inputUDH inputet / tempH) 
	(setq temph (- inputUDH ad_et ad_et))
	(cond
		((> inputUDH (cm2cu 95.0)) (gm1_createShelfDividList tempH inputet 3.0))
		((> inputUDH (cm2cu 71.0)) (gm1_createShelfDividList tempH inputet 2.0))
		((> inputUDH (cm2cu 39.0)) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil)
	)
)

(defun gm1_Alt_Raf_Kural (inputad_ADH inputet / tempH) 
	(setq temph (- inputad_ADH ad_et ad_et))
	(cond
		((> inputad_ADH (cm2cu 120.0)) (gm1_createShelfDividList tempH inputet 3.0))
		((> inputad_ADH (cm2cu 75.0)) (gm1_createShelfDividList tempH inputet 2.0))
		((> inputad_ADH (cm2cu 55.0)) (gm1_createShelfDividList tempH inputet 1.0))
		(T nil)
	)
)

;�styar�m dolap y�ksekli�i UDH2
(defun UYRM ()
	(/ ad_UDH1 2.0)
)

(defun UYRM2 ()
	(/ ad_UDH2 2.0)
)

;�styar�m dolap y�ksekli�i UDH3
(defun UYRM3 ()
	(/ ad_UDH3 2.0)
)

;�styar�m dolap y�ksekli�i UDH4
(defun UYRM4 ()
	(/ ad_UDH4 2.0)
)

;�styar�m dolap y�ksekli�i UDH5
(defun UYRM5 ()
	(/ ad_UDH5 2.0)
)

;Buzdolab� kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;Buzdolab� kabini BDH3
(defun BCH3 ()
	(+ ad_BDH3 ad_bazaH)
)

;Buzdolab� kabini BDH4
(defun BCH4 ()
	(+ ad_BDH4 ad_bazaH)
)

;Buzdolab� kabini BDH5
(defun BCH5 ()
	(+ ad_BDH5 ad_bazaH)
)

;Set �st� dolap �l��s�
(defun SETUSTU ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH)
)

;Set �st� dolap �l��s� UDH2 
(defun SETUSTU2 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH2)
)

;Set �st� dolap �l��s� UDH3 
(defun SETUSTU3 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH3)
)

;Set �st� dolap �l��s� UDH4 
(defun SETUSTU4 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH4)
)

;Set �st� dolap �l��s� UDH5 
(defun SETUSTU5 ()
	(+ (- ad_UDY ad_ADH ad_TezH ad_bazaH) ad_UDH5)
)

;____________
;Boy dolap 
(defun bh_boy_#1b ()
	(- (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5)))  g_ClearWallZBot )
)

(defun kh_boy_#1b ()
	;(+ (- remCTH (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	;(- (- (+ (bh_boy_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5)) (* (- g_ClearWallZBot g_ClearWallZBtwn) 0.5))
	(- (+ (bh_boy_#1b) (* ad_et 1.5) ) ZTop (* Zbtwn 0.5))
)

(defun bh_boy_#2b ()
	(+ (- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ad_et 1.5) ) g_ClearWallZBot )
)

(defun kh_boy_#2b ()
	;(- (- ad_UDY ad_Bazah (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot)
	(- (+ (bh_boy_#2b) (* ad_et 1.5) ) ZBot (* Zbtwn 0.5))
)

;________
;Boy dolap �st uzun kapakl� �st b�l�m Alt b�l�m alt mod�l kapa�� gibi �al���r
(defun bh_boy2_#1b ()
	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- g_ClearBaseZTop g_ClearWallZBtwn))
)
(defun kh_boy2_#1b ()
;	(+ (- remCTH (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et ZBot (* Zbtwn 0.5)))
	(- (+ (bh_boy2_#1b) (* ad_et 1.5)) ZTop (* ZBtwn 0.5))
)

(defun bh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ad_et 1.5) ) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

(defun kh_boy2_#2b ()
	(- (- (- ad_ADH (* ZBtwn 0.5) ) (* ZBtwn 0.5) ZBot) (- g_ClearBaseZTop g_ClearWallZBtwn))
)

;Boy F�r�n mod�l� iki �ekmeceli �ekmece b�l�m�
;�st �ekmece
(defun bh_bf2c_#2b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBot) 0.50) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf2c_#2b) ZBtwn) (* ad_et 0.5)) 
)

(defun kh_bf2c_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.5))
)

;alt �ekmece
(defun bh_bf2c_#3b ()
	(- (+ (kh_bf2c_#2b) (* ZBtwn 0.5) ZBot) ad_et)
)

;Boydolap �� �ekemeceli mod�l�m dar �ekmecesi kapak bo�luk t�laraslar� kapa�a yedirilmi ve kapaklara payla�t�r�lm��t�r.
(defun bh_bf3c_#1b ()
	;(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) ZBtwn) (* ad_et 0.5))
	(- (+ (kh_bf3c_#1b) ZBtwn) (* ad_et 0.5))
)

(defun bh_bf3c_#2b ()	
	 (+ (kh_bf3c_#1b) ZBtwn) 
)

(defun kh_bf3c_#1b ()
	(- (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.25) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.25))
)

;Boydolap �� �ekemeceli mod�l�m alt b�y�k �ekmecesi kapak bo�luk t�laraslar� kapa�a yedirilmi ve kapaklara payla�t�r�lm��t�r.
(defun bh_bf3c_#3b ()
	(- (+ (kh_bf3c_#3b) (* ZBtwn 0.5) Zbot)  ad_et )
)
(defun kh_bf3c_#3b ()
	(- (+ (* (- (- ad_ADH (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBtwn ZBtwn ZBtwn ZBot) 0.50) ZBtwn) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Boy F�r�n Mikrodalgal� mod�l �st kapak b�l�m
(defun bh_bfmd_#1b ()
	 (- remCTH (bh_bfmd_#2b)) 
)

(defun kh_bfmd_#1b ()
	(+ (- ( bh_bfmd_#1b ) Ztop (* ZBtwn 0.5) ) (* ad_et 1.5))
)

;Boy F�r�n Mikrodalga alt sabit �ekmeceli
(defun bh_bfmd_#2b ()
	(- (+ (kh_bfmd_#2b) (* ZBtwn 0.5) Zbot) (* ad_et 1.5))
)

(defun kh_bfmd_#2b ()
	(- (* (- ad_ADH ZTop ZBtwn ZBot) 0.50) (* (- g_ClearBaseZTop g_ClearWallZBtwn) 0.50))
)

;Buzdolab� kabini BDH2
(defun BCH2 ()
	(+ ad_BDH2 ad_bazaH)
)

;________
;Yar�mboy dolap y�ksekli�i
(defun YBOY ()
	(- ad_UDY ad_bazaH)
)

;Yar�mboy dolap i�i 
(defun gm1_YBOY_RAF ()
	(gm1_createShelfDividList curDivH ad_et 3.0)
)

;Yar�mboy tek kapakl� dolap
(defun bh_yboy ()
	 remCTH
)
(defun kh_yboy ()
	 (- H ZTop ZBot)
)

;_______
;Yar�mboy F�r�n Mikrodalga alt sabit �ekmeceli
;�st b�l�m sabit parca 
(defun bh_ybd_#1b ()
	(- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5)))
)

(defun kh_ybd_#1b ()
	(+ (- remCTH (- (- (* ad_ADH 0.25) (* ZBtwn 0.5) ) (* ad_et 1.5))) (- ad_et Ztop) )
)

(defun bh_ybd_#2b ()
	(- (* ad_ADH 0.25) (* ZBtwn 0.5) (* ad_et 1.5))
)

(defun kh_ybd_#2b ()
	(- (- (* ad_ADH 0.25) (* ZBtwn 0.5)) (* ZBtwn 0.5) ZBot)
)

;_______
;bardakl�kl�k
(defun Ust_Brdk_b1 (inputUDH / temph)
	(setq temph (- inputUDH ad_et ad_et ad_et (cm2cu 27.15)))
)
	
(defun Ust_Brdk_k1 (inputUDH / temph)
	(setq temph (- (- inputUDH (* ad_et 0.5) ad_et 27.15) (* ZBtwn 0.5) Ztop))
)


;;;;;;;;;;;;;;;;;;;;;;; GM1 Temel Fonksiyonlar
; b�l�m kalan� hesaplama (b�l�m numaras� ile kullan�l�r)  ;{(GM1_restH 1)}
(defun GM1_restH (divIndex) 
	(setq divHFormulation (strcat "{(GM1_restH " (rtos divIndex) ")}"))
	(if mozel_mUnit
		(rtos (gm1_calcRestH (- divIndex 1)) 2 4)
		(rtos (gm1_calcRestH_Selective (- divIndex 1) (if BDH BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL)))) (if divisionList divisionList (cadr (getnth 6 (assoc kod mLofL))))) 2 4)
	)
)

; kapak kalan� hesaplama (b�l�m numaras� ile kullan�l�r)  ;{(GM1_restDoorH 1)}
(defun GM1_restDoorH (divIndex)
	(setq doorHFormulation (strcat "{(GM1_restDoorH " (rtos divIndex) ")}"))
	(if mozel_mUnit
		(rtos (gm1_calcRestDoorH (- divIndex 1)) 2 4)
		(rtos (gm1_calcRestDoorH_Selective (- divIndex 1) (if BDH BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL)))) (if divisionList divisionList (cadr (getnth 6 (assoc kod mLofL))))) 2 4)
	)
)

;gola wyswyg scriptlerinde kullan�lmas� i�in yaz�ld�. kullan�m -> (gm1_divHei 2 0.0) -> 2. b�l�m�n �st k�sm�n�n module g�re y�ksekli�ini bulur ve bu y�ksekli�e 0.0 offsetini ekleyip geri d�nd�r�r.
(defun gm1_divHei (divNo heiOffset / totalHei divisionInfo i) 	
	(setq divNo (- divNo 1) totalHei 0.0 i 0)
	(while (>= divNo i)
		(setq totalHei (+ totalHei (lmm_atofFormula (if (equal i 0) ad_et (ifnull (cadr (getnth (- i 1) divisionList)) ad_et)))))
		(setq i (+ i 1))
	)
	(+ (- BDH totalHei) heiOffset)
)

;gm1 b�l�m y�ksekli�i de�erini fonksiyona �evir. kullan�m -> (gm1_divNoHei 2 0.0)
 (defun gm1_divNoHei (divNo)
	(if mozel_mUnit
		(lmm_atofFormula (cadr (getnth (- divNo 1) newDivs)))
		(lmm_atofFormula (cadr (getnth (- divNo 1) (if divisionList divisionList (cadr (getnth 6 (assoc kod mLofL)))))))
	)
)

;gm1 mod�l y�ksekli�i de�erini fonksiyona �evir
(defun gm1_curUnitH () 
	(if mozel_mUnit 
	newH 
	(if BDH BDH (lmm_atofFormula (getnth 3 (assoc kod mLofL))))
	)
)


;;;;;;;;;;;;;;;;;;;;;;; KENARBANT
(setq PvcKoduKAPAK "PVC1mm")		;GENEL KAPAK BANDI �SM�
(setq PvcKoduGENEL "PVC04mm")		;GENEL KENAR BANDI �SM�
(setq PvcKoduUST "PVC04mm")		;UST TARAFLAR
(setq PvcKoduALT "PVC04mm")		;ALT TARAFLAR
(setq PvcKoduYAN "PVC04mm")		;YAN TARAFLAR
(setq PvcKoduON "PVC04mm")		;ON TARAFLAR
(setq PvcKoduARKA nil)			;ARKA TARAFA BAKAN
(setq PvcKoduCONN nil)			;BA�LAYICI GELEN YERE BANT �SM�
(setq PvcKodu45CONN nil)		;45 B�RLE��M GELEN YERE BANT �SM�

(setq PvcKoduHRafYan "PVC04mm")		;HAREKETL� RAF YAN TARAFLAR
(setq PvcKoduHRafArka "PVC04mm")	;HAREKETL� RAF ARKA TARAFLAR
(setq PvcKoduCekUst "PVC04mm")		;CEKMECE UST TARAFLAR
(setq PvcKoduCekAlt nil)		;CEKMECE ALT TARAFLAR

