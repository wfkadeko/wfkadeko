(defun replaceKapak_pre_reserved (doorNamee kulpPoss upmodee)
	(setmatmgrsearchfilter (strcat doorNamee "_MAT"))
	; (setmatmgrsearchfilter "")
	(if (null (setq g_myselectedDoorMat (car (choosematerial)))) (exit))
	(setmatmgrsearchfilter "")
	
	(cond
		((equal "tum" upmode) (SetKapakMat g_myselectedDoorMat) (SetUpKapakMat g_myselectedDoorMat) (SetTallKapakMat g_myselectedDoorMat))
		((equal "alt" upmode) (SetKapakMat g_myselectedDoorMat))
		((equal "ust" upmode) (SetUpKapakMat g_myselectedDoorMat))
		((equal "boy" upmode) (SetTallKapakMat g_myselectedDoorMat))
	)
)

(defun updateModelOfKapak_post_reserved (inss kapakEnamee doorNamee kapakBlockNamee kapakFormm)
	(if (and g_myselectedDoorMat (equal upmode "ozel"))
		(set_block_material kapakEnamee T "CAB_DOORS" g_myselectedDoorMat)
	)
)

(defun c:matmgr->selective ( / selectedTemp kanattemp blkList fromEnt blk zEN tip kulp EN BOY bnam doorName tmp z_dCornAng Zbw2 Zdy ZWid yPa kulpSeri finalMode selectedLayer tempResult)
		
	(setmatmgrsearchenablestate 1)
	(setmatmgrsearchfilter "")
	(defun funOnCloseAtMatManager ()
		;(setmatmgrsearchenablestate 1)
		(setmatmgrsearchfilter "")
		(setq g_matfilterSS nil)
		(matmgr_triggers)
		(setLayersOn "")
		(displayFlush) 
		(displayLiberate)			
	)	
	
	(defun c:matmgr ( / selectedTemp myModulCode donotRunMatmgrOrj myDoorMat selectedModul myChosenMat onSelectionError materialRule materialLengthLimit doNotPaintThePlinths devam nextDoorName)
		(setq selectedTempFull  (rout_pickEntWithParent) donotRunMatmgrOrj nil onSelectionError nil)		
		(if (null (getnth 0 selectedTempFull))
			(progn
				(setq onSelectionError T)
				(if (yesno "There is no object at the point you selected. Would you like to make a material inquiry?")
					(progn
						(c:mogren)							
					)
				)
			)
			(progn	
				(setq selectedModul (last (last (getnth 0 selectedTempFull))))	
				(if (not (equal (type selectedModul) 'ENAME)) (setq selectedModul nil))
				(setq selectedTemp	(getnth 1 selectedTempFull)   selectedFace (getnth 1 selectedTempFull)   )
				(setq g_matfilterSS (ssadd selectedTemp) )
				(if (setq selectedLayer (dfetch 8 (entget selectedTemp))) (ssadd (tblobjname "LAYER" selectedLayer) g_matfilterSS))
			
			)
		)
		
		(cond
			((and (dfetch 330 (entget selectedTemp)) (equal (dfetch 8 (entget selectedTemp)) "CAB_DOORS"))
				(setq kanattemp  (dfetch 2 (dfetch 330 selectedTemp)))							
				(if (regexMatch kanattemp "K.{3,}_+[0-9]{4}X[0-9]{4}.*")
					(kpk:getData kanattemp 'tip 'kulp 'doorName 'EN 'BOY 'tmp 'Zbw2 'Zdy 'ZWid)
					(setq doorName (getkapakname))
				)
				
				(setmatmgrsearchfilter (strcat doorName "_MAT"))
				(setq myDoorMat (car (choosematerial)))							
				;(switchLayersOnOff "CAB_DOORS")
				(if myDoorMat
					(set_block_material selectedModul T "CAB_DOORS" myDoorMat)
				)
				(redraw)
				(setq donotRunMatmgrOrj T)
				(displayFlush)
				(displayLiberate)
				
				(setq devam T)
				(while devam
					(setq devam nil selectedTempFull  (rout_pickEntWithParent))
					(if (car selectedTempFull)
						(progn
							(setq selectedModul (last (last (getnth 0 selectedTempFull))))	
							(if (not (equal (type selectedModul) 'ENAME)) (setq selectedModul nil))
							(setq selectedTemp	(getnth 1 selectedTempFull)   selectedFace (getnth 1 selectedTempFull)   )
							(setq g_matfilterSS (ssadd selectedTemp) )
							(if (setq selectedLayer (dfetch 8 (entget selectedTemp))) (ssadd (tblobjname "LAYER" selectedLayer) g_matfilterSS))
							
							(if (and (dfetch 330 (entget selectedTemp)) (equal (dfetch 8 (entget selectedTemp)) "CAB_DOORS"))
								(progn
									(setq kanattemp  (dfetch 2 (dfetch 330 selectedTemp)))							
									(if (regexMatch kanattemp "K.{3,}_+[0-9]{4}X[0-9]{4}.*")
										(kpk:getData kanattemp 'tip 'kulp 'nextDoorName 'EN 'BOY 'tmp 'Zbw2 'Zdy 'ZWid)
										(setq nextDoorName (getkapakname))
									)
									(if (equal nextDoorName doorName)
										(progn
											(if myDoorMat
												(progn
													(set_block_material selectedModul T "CAB_DOORS" myDoorMat)
													(setq devam T)
												)
											)
											(redraw)
											(setq donotRunMatmgrOrj T)
											(displayFlush)
											(displayLiberate)
										)
										(alert "This is not same door model!")
									)
								)
								(alert "This component is not a door!")
							)
						)
					)
				)
				
			)
			((member (dfetch 8 selectedFace)   (list "CAB_BODY_BASE" "CAB_BODY_TALL" "CAB_BODY_WALL"))
				(setmatmgrsearchfilter "CAB_BODY")
			)
			((member (dfetch 8 selectedFace) (list "PANELS_SIDE" "SHELVES" "PANELS_WALL_2ND_BACK" "PANEL_WALL_DRAWER" "WARDROBE_FRAME" "PANELS_WALL" "RSIDE" "LSIDE"))
				(setmatmgrsearchfilter "PANELMAT")
			)
		)
		(if (null donotRunMatmgrOrj) (c:matmgr_orj))
	)
	(princ)
)

(c:matmgr->selective)

(defun matmgr_triggers ( / selectedKapakMat)
	(princ)
)




