(_RUNDEFAULTHELPERRCP "AKK_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "DOORS_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "HINGE_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "OPERATIONS_DEFAULT" nil "p2chelper")

; States of hinge connection
; Modul Direction		Door Open Direction			Panel which hinge connects to
;		Left					Left					Extra Part or Blind Door
;		Left					Right				 		Right Panel
;		Right					Left						Left Panel
;		Right					Right					Extra Part or Blind Door
(if (or (_NOTNULL USE_PROJECT_PRICING_RECIPES) (null BLIND_CORNER_CONTROL))
	(progn
		; If project pricing recipes will be used instead of operations or module type validation fails then there is no need for notch operations also
		(_NONOTCH)
	)
	(progn
		; Control for item variables
		(if (equal DO_NOT_ADD_CONN_AS_ITEM T)
			(_FSET (_ 'QUANTITY_OF_ITEM 0))
			(if (equal DO_NOT_ADD_HINGE_SCREW_AS_ITEM T)
				(_FSET (_ 'QUANTITY_OF_ITEM 0))
				(_FSET (_ 'QUANTITY_OF_ITEM HINGE_SCREW_QUAN))
			)
		)
		(_FSET (_ 'distOfConnHolesToCenter HINGE_PARAM_VER))
		(_FSET (_ 'connHolesHorDiff HINGE_PARAM_HOR))
		
		(_FSET (_ 'doorCounter 0))
		(repeat __DOORSCOUNT
			(_FSET (_ 'doorCounter (+ 1 doorCounter)))
			(_FSET (_ 'paramBody (_& (_ "AKK_CDOOR_" doorCounter "_"))))
			
			(_FSET (_ 'currentDoorTYPE (_S2V (_& (_ paramBody "TYPE")))))
			; Control for drawer
			(if (and (_ISDOORDRAWER currentDoorTYPE) (null ASSUME_DRAWER_AS_DOOR))
				(_FSET (_ 'drawerControl nil))
				(_FSET (_ 'drawerControl T))
			)
			; If drawer control fails, no part of operation is performed
			(if (_NOTNULL drawerControl)
				(progn
					; Current door variables
					(_FSET (_ 'currentDoorHEI (_S2V (_& (_ paramBody "HEI")))))
					(_FSET (_ 'currentDoorELEV (_S2V (_& (_ paramBody "ELEV")))))
					(_FSET (_ 'currentDoorODIR (_S2V (_& (_ paramBody "ODIR")))))
					(_FSET (_ 'currentDoorFRAME (_S2V (_& (_ paramBody "FRAME")))))
					(if (not (apply 'OR currentDoorFRAME))
						(progn
							(_FSET (_ 'topFrameVAL 0))
							(_FSET (_ 'rightFrameVAL 0))
							(_FSET (_ 'bottomFrameVAL 0))
							(_FSET (_ 'leftFrameVAL 0))
						)
						(progn
							(_FSET (_ 'topFrameVAL (getnth 0 currentDoorFRAME)))
							(_FSET (_ 'rightFrameVAL (getnth 1 currentDoorFRAME)))
							(_FSET (_ 'bottomFrameVAL (getnth 2 currentDoorFRAME)))
							(_FSET (_ 'leftFrameVAL (getnth 3 currentDoorFRAME)))
						)
					)
					
					; DOOR OPERATION
					(_FSET (_ 'currentDoorCODE (_CREATEDOORCODE currentDoorTYPE __CURDIVORDER doorCounter CHANGE_BCU_DOOR_CODES "BCU")))
					(if (not (_EXISTPANEL currentDoorCODE))
						(progn
							; There is no door which is eligible for operation, then situation of items have to be checked
							(if (_NOTNULL (_S2V (_& (_ paramBody "VIRTUAL")))) 
								(if (equal doorCounter 2)
									(_ITEMMAIN HINGE1_CODE HINGE1_CODE (_ AKK_HINGE_QUAN HINGE_UNIT))
									(_ITEMMAIN AKK_HINGE_CODE AKK_HINGE_CODE (_ AKK_HINGE_QUAN HINGE_UNIT))
								)
							)
						)
						(progn
							(_FSET (_ 'reqThicknessInside (+ HINGE_OFFSET BIG_TO_SMALLS_OFFSET_X (/ SMALL_HOLES_DIAMETER 2.0))))
							(_FSET (_ 'reqThicknessOutside (- HINGE_OFFSET (/ BIG_HOLE_DIAMETER 2.0))))
							
							(_FSET (_ 'frameControl (_CONTROLFRAME paramBody currentDoorTYPE reqThicknessInside reqThicknessOutside nil)))
							(if (null frameControl)
								(progn
									(alert (strcat (XSTR "MODULE CODE") " : " __MODULCODE "\t" (XSTR "DOOR CODE") " : " currentDoorCODE "\n" "\n" 
												   (XSTR "Blind corner hinge operation can not be performed on door frame with current parameters!") "\n"))
								)
								(progn
									(_FSET (_ 'innerBoundry (+ HINGE_OFFSET BIG_TO_SMALLS_OFFSET_X (/ SMALL_HOLES_DIAMETER 2.0))))
									(_FSET (_ 'outerBoundry (- HINGE_OFFSET (/ BIG_HOLE_DIAMETER 2.0))))
									; According to open direction of current door, frame control is performed
									(_FSET (_ 'frameControl (_CONTROLFRAME paramBody currentDoorTYPE innerBoundry outerBoundry nil)))
									(if (null frameControl)
										(progn
											(alert (strcat (XSTR "MODULE CODE") " : " __MODULCODE "\t" (XSTR "DOOR CODE") " : " currentDoorCODE "\n" "\n" 
														   (XSTR "Blind corner hinge operation can not be performed on door frame with current parameters!") "\n"))
										)
										(progn
											(if (null NO_NEED_HINGE_HOLES_ON_DOORS)
												(progn
													(_FSET (_ 'currentDoorWID (_S2V (_& (_ paramBody "WID")))))
													; According to open direction of current door, position of hinges are determined
													(cond
														((equal currentDoorODIR "L")
															(_FSET (_ 'hingeDirection "X+"))
															
															(_FSET (_ 'upperHingePOS (_ (_= "currentDoorWID + leftFrameVAL - HINGE_OFFSET") (_= "currentDoorHEI + topFrameVAL - HINGE_DISTANCE") 0)))
															(_FSET (_ 'lowerHingePOS (_ (_= "currentDoorWID + leftFrameVAL - HINGE_OFFSET") (- HINGE_DISTANCE bottomFrameVAL) 0)))
														)
														((equal currentDoorODIR "R")
															(_FSET (_ 'hingeDirection "X-"))
															
															(_FSET (_ 'upperHingePOS (_ (- HINGE_OFFSET rightFrameVAL) (_= "currentDoorHEI + topFrameVAL - HINGE_DISTANCE") 0)))
															(_FSET (_ 'lowerHingePOS (_ (- HINGE_OFFSET rightFrameVAL) (- HINGE_DISTANCE bottomFrameVAL) 0)))
														)
													)
													(_HINGEMAIN "HINGE_UP" currentDoorCODE (_ upperHingePOS hingeDirection BIG_HOLE_DIAMETER SMALL_HOLES_DIAMETER BIG_TO_SMALLS_OFFSET_X BIG_TO_SMALLS_OFFSET_Y BIG_HOLE_DEPTH SMALL_HOLES_DEPTH))
													(_HINGEMAIN "HINGE_DOWN" currentDoorCODE (_ lowerHingePOS hingeDirection BIG_HOLE_DIAMETER SMALL_HOLES_DIAMETER BIG_TO_SMALLS_OFFSET_X BIG_TO_SMALLS_OFFSET_Y BIG_HOLE_DEPTH SMALL_HOLES_DEPTH))
													
													(_ITEMMAIN HINGE_SCREW_CODE currentDoorCODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
												)
											)
											(if (equal doorCounter 2)
												(_ITEMMAIN HINGE1_CODE currentDoorCODE (_ AKK_HINGE_QUAN HINGE_UNIT))
												(_ITEMMAIN AKK_HINGE_CODE currentDoorCODE (_ AKK_HINGE_QUAN HINGE_UNIT))
											)
										)
									)
								)
							)
						)
					)
					; SIDE OPERATION
					(if (null NO_NEED_HINGE_CONN_HOLES)
						(progn
							(_FSET (_ 'holeCodeBody "HINGE_CONN_HOLE_"))
							(cond
								((equal __MODULDIRECTION currentDoorODIR)
									; Open direction of current door is same with the direction of module
									; Modul Direction		Door Open Direction			Panel which hinge connects to
									;		Left					Left					Extra Part or Blind Door
									;		Right					Right					Extra Part or Blind Door
									
									; IMPORTANT INFORMATION
									; AKK_PARTS_LOSSES is variable which is set in AKK_MANUFACTURER.p2c file. It is not a variable which can be accessed via user interface
									
									(cond
										((_NOTNULL IS_INNER_EXTRA_PART_AVAILABLE)
											; INNER EXTRA PART
											(_FSET (_ 'connPanelCODE AKK_INNER_EXTRA_PART_CODE))
											(_FSET (_ 'connPanelWID INNER_EXTRA_PART_WID))
											
											(_FSET (_ 'bottomSideLOSS __AD_PANELTHICK)) ; inner space
										)
										((and (_NOTNULL IS_EXTRA_PART_AVAILABLE) (< OVERLAPPING_DISTANCE INNER_EXTRA_PART_WID))
											; EXTRA PART
											(_FSET (_ 'connPanelCODE AKK_EXTRA_PART_CODE))
											(_FSET (_ 'connPanelWID AKK_EXTRA_PART_WID))
											
											(_FSET (_ 'bottomSideLOSS (getnth 3 AKK_PARTS_LOSSES)))
										)
										(T
											; BLIND DOOR
											(_FSET (_ 'connPanelCODE AKK_BLIND_DOOR_CODE))
											(_FSET (_ 'connPanelWID AKK_BLIND_DOOR_WID))
											
											(_FSET (_ 'bottomSideLOSS (getnth 1 AKK_PARTS_LOSSES)))
										)
									)
									; According to current door open direction, position of holes are determined
									(cond
										((equal currentDoorODIR "L")
											; Meaning of shortkeyes 	U->Up, D->Down, F->First, S->Second
											(_FSET (_ 'connHolePOS_UF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideLOSS + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_US (_ (+ HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideLOSS + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_DF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideLOSS + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_DS (_ (+ HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideLOSS + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
										)
										((equal currentDoorODIR "R")
											(_FSET (_ 'connHolePOS_UF (_ (- connPanelWID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideLOSS + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_US (_ (_= "connPanelWID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideLOSS + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_DF (_ (- connPanelWID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideLOSS + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
											(_FSET (_ 'connHolePOS_DS (_ (_= "connPanelWID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideLOSS + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
										)
									)
									(_HOLE (_& (_ holeCodeBody "UF")) connPanelCODE (_ connHolePOS_UF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
									(_HOLE (_& (_ holeCodeBody "US")) connPanelCODE (_ connHolePOS_US HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
									(_HOLE (_& (_ holeCodeBody "DF")) connPanelCODE (_ connHolePOS_DF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
									(_HOLE (_& (_ holeCodeBody "DS")) connPanelCODE (_ connHolePOS_DS HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
									
									(_ITEMMAIN HINGE_SCREW_CODE connPanelCODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
								)
								(T
									; Open direction of current door is different from module direction
									; Modul Direction		Door Open Direction			Panel which hinge connects to
									;		Left					Right				 		Right Panel
									;		Right					Left						Left Panel
									(cond 
										((equal currentDoorODIR "L")
											(if (_EXISTPANEL LEFT_PANEL_CODE)
												(progn
													(_FSET (_ 'connHolePOS_UF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_US (_ (+ HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_DF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_DS (_ (+ HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
													
													(_HOLE (_& (_ holeCodeBody "UF")) LEFT_PANEL_CODE (_ connHolePOS_UF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "US")) LEFT_PANEL_CODE (_ connHolePOS_US HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "DF")) LEFT_PANEL_CODE (_ connHolePOS_DF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "DS")) LEFT_PANEL_CODE (_ connHolePOS_DS HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													
													(_ITEMMAIN HINGE_SCREW_CODE LEFT_PANEL_CODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
												)
											)
										)
										((equal currentDoorODIR "R")
											(if (_EXISTPANEL RIGHT_PANEL_CODE)
												(progn
													(_FSET (_ 'connHolePOS_UF (_ (- RIGHT_PANEL_WID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_US (_ (_= "RIGHT_PANEL_WID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_DF (_ (- RIGHT_PANEL_WID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
													(_FSET (_ 'connHolePOS_DS (_ (_= "RIGHT_PANEL_WID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
													
													(_HOLE (_& (_ holeCodeBody "UF")) RIGHT_PANEL_CODE (_ connHolePOS_UF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "US")) RIGHT_PANEL_CODE (_ connHolePOS_US HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "DF")) RIGHT_PANEL_CODE (_ connHolePOS_DF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													(_HOLE (_& (_ holeCodeBody "DS")) RIGHT_PANEL_CODE (_ connHolePOS_DS HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
													
													(_ITEMMAIN HINGE_SCREW_CODE RIGHT_PANEL_CODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)
)
(if (_NOTNULL NO_NEED_HINGE_CONN_HOLES) (_NONOTCH))