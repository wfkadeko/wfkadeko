(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "DOORS_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "HINGE_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "OPERATIONS_DEFAULT" nil "p2chelper")

; State of hinge connections 
;	Modul Direction			Door open Direction			Hinge connection Panel
;	Right					Right                       Short Panel
;	Right                   Left                        Long Panel
;	Left                    Right                       Long Panel
;	Left                    Left                        Short Panel

(if (and (null USE_PROJECT_PRICING_RECIPES) (_NOTNULL OBLIQUE_UNIT_CONTROL))
	(progn
		(if (equal DO_NOT_ADD_CONN_AS_ITEM T)
			(_FSET (_ 'QUANTITY_OF_ITEM 0))
			(if (equal DO_NOT_ADD_HINGE_SCREW_AS_ITEM T)
				(_FSET (_ 'QUANTITY_OF_ITEM 0))
				(_FSET (_ 'QUANTITY_OF_ITEM HINGE_SCREW_QUAN))
			)
		)
		(_FSET (_ 'distOfConnHolesToCenter HINGE_PARAM_VER))
		(_FSET (_ 'connHolesHorDiff HINGE_PARAM_HOR))
		
		(_FSET (_ 'doorCounter 0))
		(repeat (length __CURDIVDOORSLIST)
			(_FSET (_ 'doorCounter (+ 1 doorCounter)))
			(_FSET (_ 'paramBody (_& (_ OBLIQUE_DOOR_PARAM_ROOT "_" doorCounter "_"))))
			
			; Current door parameters
			(_FSET (_ 'currentDoorTYPE (_S2V (_& (_ paramBody "TYPE")))))
			(_FSET (_ 'currentDoorHEI (_S2V (_& (_ paramBody "HEI")))))
			(_FSET (_ 'currentDoorWID (_S2V (_& (_ paramBody "WID")))))
			(_FSET (_ 'currentDoorELEV (_S2V (_& (_ paramBody "ELEV")))))
			(_FSET (_ 'currentDoorODIR (_S2V (_& (_ paramBody "ODIR")))))
			(_FSET (_ 'currentDoorFRAME (_S2V (_& (_ paramBody "FRAME")))))
			
			(_FSET (_ 'topFrameVAL (getnth 0 currentDoorFRAME)))
			(_FSET (_ 'rightFrameVAL (getnth 1 currentDoorFRAME)))
			(_FSET (_ 'bottomFrameVAL (getnth 2 currentDoorFRAME)))
			(_FSET (_ 'leftFrameVAL (getnth 3 currentDoorFRAME)))
			
			; DOOR OPERATION
			(_FSET (_ 'currentDoorCODE (_CREATEDOORCODE currentDoorTYPE __CURDIVORDER doorCounter CHANGE_OB_DOOR_CODES OBLIQUE_UNIT_DEF)))
			(if (not (_EXISTPANEL currentDoorCODE))
				(progn
					; There is no door which is eligible for operation, then situation of items have to be checked
					(if (_NOTNULL (_S2V (_& (_ paramBody "VIRTUAL")))) 
						(_ITEMMAIN (car HINGE_CODES) (car HINGE_CODES) (car QUANTITY_PARAMS))
						(_ITEMMAIN (cadr HINGE_CODES) (cadr HINGE_CODES) (cadr QUANTITY_PARAMS))
					)
				)
				(progn
					(_FSET (_ 'innerBoundry (+ HINGE_OFFSET BIG_TO_SMALLS_OFFSET_X (/ SMALL_HOLES_DIAMETER 2.0))))
					(_FSET (_ 'outerBoundry (- HINGE_OFFSET (/ BIG_HOLE_DIAMETER 2.0))))
					
					(_FSET (_ 'frameControl (_CONTROLFRAME paramBody currentDoorTYPE innerBoundry outerBoundry nil)))
					(if (null frameControl)
						(progn
							; Detailed module definition is determined for warning
							(cond
								((equal OBLIQUE_UNIT_DEF "WO")
									(_FSET (_ 'detailedModuleDEF (XSTR "Wall Oblique")))
								)
								((equal OBLIQUE_UNIT_DEF "TO")
									(_FSET (_ 'detailedModuleDEF (XSTR "Tall Oblique")))
								)
							)
							(alert (strcat (XSTR "MODULE CODE") " : " __MODULCODE "\t" (XSTR "DOOR CODE") " : " currentDoorCODE "\n" "\n" 
										   detailedModuleDEF (XSTR " hinge operation can not be performed on door frame with current parameters!") "\n"))
						)
						(progn
							(if (null NO_NEED_HINGE_HOLES_ON_DOORS)
								(progn
									; According to open direction of current door, position of hinges are determined
									(cond
										((equal currentDoorODIR "L")
											(_FSET (_ 'hingeDirection "X+"))
											
											(_FSET (_ 'upperHingePOS (_ (_= "currentDoorWID + leftFrameVAL - HINGE_OFFSET") (_= "currentDoorHEI + topFrameVAL - HINGE_DISTANCE") 0)))
											(_FSET (_ 'lowerHingePOS (_ (_= "currentDoorWID + leftFrameVAL - HINGE_OFFSET") (- HINGE_DISTANCE bottomFrameVAL) 0)))
										)
										((equal currentDoorODIR "R")
											(_FSET (_ 'hingeDirection "X-"))
											
											(_FSET (_ 'upperHingePOS (_ (- HINGE_OFFSET rightFrameVAL) (_= "currentDoorHEI + topFrameVAL - HINGE_DISTANCE") 0)))
											(_FSET (_ 'lowerHingePOS (_ (- HINGE_OFFSET rightFrameVAL) (- HINGE_DISTANCE bottomFrameVAL) 0)))
										)
									)
									(_HINGEMAIN "HINGE_UP" currentDoorCODE (_ upperHingePOS hingeDirection BIG_HOLE_DIAMETER SMALL_HOLES_DIAMETER BIG_TO_SMALLS_OFFSET_X BIG_TO_SMALLS_OFFSET_Y BIG_HOLE_DEPTH SMALL_HOLES_DEPTH))
									(_HINGEMAIN "HINGE_DOWN" currentDoorCODE (_ lowerHingePOS hingeDirection BIG_HOLE_DIAMETER SMALL_HOLES_DIAMETER BIG_TO_SMALLS_OFFSET_X BIG_TO_SMALLS_OFFSET_Y BIG_HOLE_DEPTH SMALL_HOLES_DEPTH))
									
									(_ITEMMAIN HINGE_SCREW_CODE currentDoorCODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
								)
							)
							(_ITEMMAIN (car HINGE_CODES) currentDoorCODE (car QUANTITY_PARAMS))
							(_ITEMMAIN (cadr HINGE_CODES) currentDoorCODE (cadr QUANTITY_PARAMS))
						)
					)
				)
			)
			; SIDE OPERATION
			(if (null NO_NEED_HINGE_CONN_HOLES)
				(progn
					(_FSET (_ 'holeCodeBody "HINGE_CONN_HOLE_"))
					; Side operations state tablet
					;	Modul Direction			Door open Direction		Panel which hinge connects
					;		Left                    Left                       	Short
					;		Left                    Right                      	Long
					;		Right					Right                      	Short
					;		Right                   Left                       	Long
					
					; Side panel and its parameters related with operation is determined
					(cond
						((equal __MODULDIRECTION currentDoorODIR)
							; SHORT SIDE
							(_FSET (_ 'connPanelCODE UE_SHORT_SIDE_PANEL_CODE))
							(_FSET (_ 'connPanelWID UE_SHORT_SIDE_PANEL_WID))
						)
						(T
							; LONG SIDE
							(_FSET (_ 'connPanelCODE UE_LONG_SIDE_PANEL_CODE))
							(_FSET (_ 'connPanelWID UE_LONG_SIDE_PANEL_WID))
						)
					)
					; According to door open direction, position of hinge connection holes are determined
					(cond
						((equal currentDoorODIR "L")
							(_FSET (_ 'connHolePOS_UF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_US (_ (- HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_DF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_DS (_ (- HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
						)
						((equal currentDoorODIR "R")
							(_FSET (_ 'connHolePOS_UF (_ (- connPanelWID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_US (_ (_= "connPanelWID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + topFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_DF (_ (- connPanelWID HINGE_CONNECTION_HOLES_OFFSET) (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
							(_FSET (_ 'connHolePOS_DS (_ (_= "connPanelWID - HINGE_CONNECTION_HOLES_OFFSET - connHolesHorDiff") (_= "currentDoorELEV - bottomSideStyleV1 + HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
						)
					)
					(_HOLE (_& (_ holeCodeBody "UF")) connPanelCODE (_ connHolePOS_UF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
					(_HOLE (_& (_ holeCodeBody "US")) connPanelCODE (_ connHolePOS_US HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
					(_HOLE (_& (_ holeCodeBody "DF")) connPanelCODE (_ connHolePOS_DF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
					(_HOLE (_& (_ holeCodeBody "DS")) connPanelCODE (_ connHolePOS_DS HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
					
					(_ITEMMAIN HINGE_SCREW_CODE connPanelCODE (_ QUANTITY_OF_ITEM HINGE_SCREW_UNIT))
				)
			)
		)
	)
)
(_NONOTCH)